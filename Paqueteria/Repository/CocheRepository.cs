﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Paqueteria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Paqueteria.Exceptions;

namespace Paqueteria.Repository
{
    public class CocheRepository : ICocheRepository
    {
        private readonly DBContext _context;

        public CocheRepository(DBContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Devuelve todos los coches de la base de datos
        /// </summary>
        /// <returns>Los coches en base de datos o null si no hay ninguno.</returns>
        public async Task<ActionResult<IEnumerable<CocheItem>>> GetCoches()
        {
            return await _context.CocheItems.ToListAsync();
        }
        /// <summary>
        /// Obtiene el coche que coincide con Id indicado
        /// </summary>
        /// <param name="id">Id del coche a buscar</param>
        /// <returns>Coche que coincide con el Id</returns>
        public async Task<ActionResult<CocheItem>> GetCocheById(int id)
        {
            return await _context.CocheItems.FirstOrDefaultAsync(x => x.Id == id);
        }
        /// <summary>
        /// Devuelve el coche que coincida con la matricula indicada
        /// </summary>
        /// <param name="matricula">Matricula a buscar</param>
        /// <returns>Coche con la matricula indicada</returns>
        public async Task<ActionResult<CocheItem>> GetCocheItem(string matricula)
        {
            return await _context.CocheItems.FirstOrDefaultAsync(x => x.Matricula == matricula.ToLower());
        }
        /// <summary>
        /// Actualiza un coche en la BBDD.
        /// </summary>
        /// <param name="matricula">Matricula del coche a actualizar</param>
        /// <param name="cocheItem">Datos del coche a actualizar</param>
        /// <returns>Coche actualizado</returns>
        public async Task<ActionResult<CocheItem>> PutCocheItem(CocheItem cocheItem)
        {
            _context.Entry(cocheItem).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return cocheItem;
        }
        /// <summary>
        /// Inserta un coche en la BBDD
        /// </summary>
        /// <param name="cocheItem">Coche a insertar</param>
        /// <returns>Coche insertado</returns>
        public async Task<ActionResult<CocheItem>> PostCocheItem(CocheItem cocheItem)
        {
            _context.CocheItems.Add(cocheItem);
            await _context.SaveChangesAsync();

            return cocheItem;
        }
        /// <summary>
        /// Busca en base de datos si existe un coche con esa matricula.
        /// </summary>
        /// <param name="matricula"></param>
        /// <returns></returns>
        public async Task<bool> CocheExist(string matricula)
        {
            return await _context.CocheItems.AnyAsync(x => x.Matricula == matricula);
        }
        /// <summary>
        /// Borra un coche de la BBDD
        /// </summary>
        /// <param name="matricula">Matricula del coche a borrar</param>
        /// <returns>Coche borrado</returns>
        public async Task<ActionResult<CocheItem>> DeleteCocheItem(CocheItem cocheItem)
        {
            _context.CocheItems.Remove(cocheItem);
            await _context.SaveChangesAsync();

            return cocheItem;
        }
    }
}
