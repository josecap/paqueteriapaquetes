﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Paqueteria.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paqueteria.Repository
{
    public class PaqueteRepository : IPaqueteRepository
    {
        private readonly DBContext _paqueteContext;

        public PaqueteRepository(DBContext paqueteContext)
        {
            _paqueteContext = paqueteContext;
        }
        /// <summary>
        /// Devuelve todos los paquetes en la base de datos.
        /// </summary>
        /// <returns>Paquetes en la base de datos</returns>
        public async Task<ActionResult<IEnumerable<PaqueteItem>>> GetPaqueteItems()
        {
            return await _paqueteContext.PaqueteItems.ToListAsync();
        }

        /// <summary>
        /// Retorna, si existe, los datos del paquete que coincidan con el numero.
        /// </summary>
        /// <param name="numeroPaquete"></param>
        /// <returns></returns>
        public async Task<ActionResult<PaqueteItem>> GetPaqueteByNumeroPaquete(string numeroPaquete)
        {
            return await _paqueteContext.PaqueteItems.FirstOrDefaultAsync(x => x.NumeroPaquete == numeroPaquete);
        }
        /// <summary>
        /// Actualiza un paquete en base de datos
        /// </summary>
        /// <param name="numeroPaquete">Numero de seguimiento del paquete</param>
        /// <param name="paqueteItem">El objeto paquete con las modificaciones</param>
        /// <returns>El paquete actualizado</returns>
        public async Task<ActionResult<PaqueteItem>> PutPaqueteItem(PaqueteItem paqueteItem)
        {
            _paqueteContext.Entry(paqueteItem).State = EntityState.Modified;
            await _paqueteContext.SaveChangesAsync();

            return paqueteItem;
        }
        /// <summary>
        /// Inserta un paquete en la base de datos
        /// </summary>
        /// <param name="paqueteItem">El objeto paquete a insertar</param>
        /// <returns>El paquete actualizado</returns>
        public async Task<ActionResult<PaqueteItem>> PostPaqueteItem(PaqueteItem paqueteItem)
        {
            _paqueteContext.PaqueteItems.Add(paqueteItem);
            await _paqueteContext.SaveChangesAsync();

            return paqueteItem;
        }
        /// <summary>
        /// Borra un paquete de la base de datos
        /// </summary>
        /// <param name="numeroPaquete">Numero del paquete a borrar</param>
        /// <returns>Paquete que se ha borrado</returns>
        public async Task<ActionResult<PaqueteItem>> DeletePaqueteItem(string numeroPaquete)
        {
            var paqueteItem = await _paqueteContext.PaqueteItems.FirstOrDefaultAsync(x => x.NumeroPaquete == numeroPaquete);

            _paqueteContext.PaqueteItems.Remove(paqueteItem);
            await _paqueteContext.SaveChangesAsync();

            return paqueteItem;
        }
        /// <summary>
        /// Comprueba si el paquete existe en BBDD
        /// </summary>
        /// <param name="numeroPaquete">Numero del paquete a buscar</param>
        /// <returns>True si existe y false si no existe</returns>
        public bool PaqueteItemExists(string numeroPaquete)
        {
            return _paqueteContext.PaqueteItems.Any(e => e.NumeroPaquete == numeroPaquete);
        }
    }
}
