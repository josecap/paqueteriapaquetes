﻿using Microsoft.AspNetCore.Mvc;
using Paqueteria.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Paqueteria.Repository
{
    public interface ICocheRepository
    {
        Task<ActionResult<IEnumerable<CocheItem>>> GetCoches();
        Task<ActionResult<CocheItem>> GetCocheItem(string matricula);
        Task<ActionResult<CocheItem>> GetCocheById(int id);
        Task<ActionResult<CocheItem>> PutCocheItem(CocheItem cocheItem);
        Task<ActionResult<CocheItem>> PostCocheItem(CocheItem cocheItem);
        Task<ActionResult<CocheItem>> DeleteCocheItem(CocheItem matricula);
        Task<bool> CocheExist(string matricula);
    }
}
