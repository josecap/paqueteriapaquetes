﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Paqueteria.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Paqueteria.Repository
{
    public class CoordenadaRepository : ICoordenadaRepository
    {
        private readonly DBContext _context;

        public CoordenadaRepository(DBContext coordenadaContext)
        {
            _context = coordenadaContext;
        }

        public Task<bool> CoordenadaExist(int id)
        {
            return _context.CoordenadaItems.AnyAsync(x => x.Id == id);
        }

        public async Task<ActionResult<CoordenadaItem>> DeleteCoordenadaItem(CoordenadaItem coordenada)
        {
            _context.CoordenadaItems.Remove(coordenada);
            await _context.SaveChangesAsync();

            return coordenada;
        }

        public async Task<ActionResult<IEnumerable<CoordenadaItem>>> GetCoordenadas()
        {
            return await _context.CoordenadaItems.ToListAsync();
        }

        public async Task<ActionResult<CoordenadaItem>> PostCoordenadaItem(CoordenadaItem coordenada)
        {
            _context.CoordenadaItems.Add(coordenada);
            await _context.SaveChangesAsync();

            return coordenada;
        }

        public async Task<ActionResult<CoordenadaItem>> PutCoordenadaItem(CoordenadaItem coordenada)
        {
            _context.Entry(coordenada).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return coordenada;
        }
    }
}
