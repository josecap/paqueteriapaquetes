﻿using Microsoft.AspNetCore.Mvc;
using Paqueteria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paqueteria.Repository
{
    public interface IPaqueteRepository
    {
        Task<ActionResult<IEnumerable<PaqueteItem>>> GetPaqueteItems();
        Task<ActionResult<PaqueteItem>> GetPaqueteByNumeroPaquete(string numeroPaquete);
        Task<ActionResult<PaqueteItem>> PutPaqueteItem(PaqueteItem paqueteItem);
        Task<ActionResult<PaqueteItem>> PostPaqueteItem(PaqueteItem paqueteItem);
        Task<ActionResult<PaqueteItem>> DeletePaqueteItem(string numeroPaquete);
        bool PaqueteItemExists(string numeroPaquete);
    }
}
