using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Paqueteria.Manager;
using Paqueteria.Middleware;
using Paqueteria.Models;
using Paqueteria.Repository;

namespace Paqueteria
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DBContext>(opt =>
               opt.UseInMemoryDatabase("MemoryDB"));

            //services.AddDbContext<DBContext>(opt =>
            //   opt.UseInMemoryDatabase("PaqueteList"));

            services.AddControllers();

            services.AddSwaggerGen(x => 
            {
                x.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo()
                {
                    Title = "API Paquetería Orenes"
                });
            });

            services.AddScoped<ICocheRepository, CocheRepository>();
            services.AddScoped<IPaqueteRepository, PaqueteRepository>();
            services.AddScoped<ICoordenadaRepository, CoordenadaRepository>();
            services.AddScoped<ICocheManager, CocheManager>();
            services.AddScoped<IPaqueteManager, PaqueteManager>();
            services.AddScoped<ICoordenadaManager, CoordenadaManager>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("/swagger/v1/swagger.json", "Paquetería Orenes");
            });

            app.UseRouting();

            app.UseAuthorization();

            // global error handler
            app.UseMiddleware<ErrorHandlerMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
