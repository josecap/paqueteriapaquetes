﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paqueteria.Models
{
    public class CocheItem
    {
        /// <summary>
        /// Id del coche en BBDD
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Matricula del vehiculo
        /// </summary>
        public string Matricula { get; set; }
        /// <summary>
        /// Paquetes que pertenecen al vehiculo
        /// </summary>
        public List<PaqueteItem> Paquetes { get; set; }
        /// <summary>
        /// Historial de localizaciones del vehiculo
        /// </summary>
        public List<CoordenadaItem> Localizaciones { get; set; }
    }
}
