﻿using Microsoft.EntityFrameworkCore;

namespace Paqueteria.Models
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }

        public DbSet<CocheItem> CocheItems { get; set; }
        public DbSet<CoordenadaItem> CoordenadaItems { get; set; }
        public DbSet<PaqueteItem> PaqueteItems { get; set; }
    }
}
