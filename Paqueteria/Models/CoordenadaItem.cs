﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paqueteria.Models
{
    public class CoordenadaItem
    {
        /// <summary>
        /// Id de BBDD
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Horas del punto X
        /// </summary>
        public int HorasX { get; set; }
        /// <summary>
        /// Minutos del punto X
        /// </summary>
        public int MinutosX { get; set; }
        /// <summary>
        /// Segundos del punto X
        /// </summary>
        public int SegundosX { get; set; }
        /// <summary>
        /// Orientacion del punto X N,S,E,O
        /// </summary>
        public string OrientacionX { get; set; }
        /// <summary>
        /// Horas del punto Y
        /// </summary>
        public int HorasY { get; set; }
        /// <summary>
        /// Minutos del punto Y
        /// </summary>
        public int MinutosY { get; set; }
        /// <summary>
        /// Segundos del punto Y
        /// </summary>
        public int SegundosY { get; set; }
        /// <summary>
        /// /// Orientacion del punto Y N,S,E,O
        /// </summary>
        public string OrientacionY { get; set; }
        /// <summary>
        /// Fecha de inserccion de la localización
        /// </summary>
        public DateTime FechaActualizacion { get; set; }
    }
}
