﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paqueteria.Models
{
    public class PaqueteItem
    {
        /// <summary>
        /// Id del paquete en BBDD
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Numero de seguimiento del paquete
        /// </summary>
        public string NumeroPaquete { get; set; }
        /// <summary>
        /// Id del coche que lleva el paquete
        /// </summary>
        public int CocheItemId { get; set; }
    }
}
