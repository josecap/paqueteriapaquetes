﻿using Microsoft.AspNetCore.Mvc;
using Paqueteria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paqueteria.Manager
{
    public interface IPaqueteManager
    {
        Task<ActionResult<IEnumerable<PaqueteItem>>> GetPaqueteItems();
        Task<ActionResult<PaqueteItem>> GetPaqueteByNumeroPaquete(string numeroPaquete);
        Task<ActionResult<PaqueteItem>> PutPaqueteItem(string numeroPaquete, PaqueteItem paqueteItem);
        Task<ActionResult<PaqueteItem>> PostPaqueteItem(PaqueteItem paqueteItem);
        Task<ActionResult<PaqueteItem>> DeletePaqueteItem(string numeroPaquete);
        Task<ActionResult<CocheItem>> GetCocheByNumeroPaquete(string numeroPaquete);
    }
}
