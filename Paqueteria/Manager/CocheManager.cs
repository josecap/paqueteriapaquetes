﻿using Microsoft.AspNetCore.Mvc;
using Paqueteria.Exceptions;
using Paqueteria.Models;
using Paqueteria.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paqueteria.Manager
{
    public class CocheManager : ICocheManager
    {
        private readonly ICocheRepository _cocheRepository;
        private readonly IPaqueteRepository _paqueteRepository;
        private readonly ICoordenadaRepository _coordenadaRepository;

        public CocheManager(ICocheRepository cocheRepository, IPaqueteRepository paqueteRepository, ICoordenadaRepository coordenadaRepository)
        {
            _cocheRepository = cocheRepository;
            _paqueteRepository = paqueteRepository;
            _coordenadaRepository = coordenadaRepository;
        }

        /// <summary>
        /// Borra el coche con la matricula indicada
        /// </summary>
        /// <param name="matricula">Matricula del coche a borrar</param>
        /// <returns>Coche borrado</returns>
        public Task<ActionResult<CocheItem>> DeleteCocheItem(string matricula)
        {
            var resultCoche = _cocheRepository.GetCocheItem(matricula.ToLower()).Result;

            if (resultCoche == null)
            {
                throw new AppException("El coche no existe en la BBDD.");
            }

            var deletedCoche = _cocheRepository.DeleteCocheItem(resultCoche.Value);

            return deletedCoche;
        }
        /// <summary>
        /// Devuelve el coche que coincida con la matricula indicada
        /// </summary>
        /// <param name="matricula">Matricula del vehiculo a buscar</param>
        /// <returns>Coche que coincide con la matricula</returns>
        public Task<ActionResult<CocheItem>> GetCocheItem(string matricula)
        {
            var cocheItem = _cocheRepository.GetCocheItem(matricula.ToLower());

            if (cocheItem.Result == null)
            {
                throw new AppException("No existe el coche en la base de datos.");
            }

            return cocheItem;
        }
        /// <summary>
        /// Devuelve todos los coches de la BBDD
        /// </summary>
        /// <returns>Los coches de la BBDD</returns>
        public Task<ActionResult<IEnumerable<CocheItem>>> GetCoches()
        {
            return _cocheRepository.GetCoches();
        }
        /// <summary>
        /// Inserta un coche en la BBDD
        /// </summary>
        /// <param name="cocheItem">Coche a insertar</param>
        /// <returns>Coche insertado</returns>
        public async Task<ActionResult<CocheItem>> PostCocheItem(CocheItem cocheItem)
        {
            cocheItem.Matricula = cocheItem.Matricula.ToLower();

            if (await _cocheRepository.CocheExist(cocheItem.Matricula).ConfigureAwait(false))
            {
                throw new AppException("El coche ya existe en la BBDD.");
            }

            if (cocheItem.Paquetes.Any())
            {
                foreach (var paquete in cocheItem.Paquetes)
                {
                    await _paqueteRepository.PostPaqueteItem(paquete);
                }
            }

            if (cocheItem.Localizaciones.Any())
            {
                foreach (var localization in cocheItem.Localizaciones)
                {
                    await _coordenadaRepository.PostCoordenadaItem(localization);
                }
            }

            return await _cocheRepository.PostCocheItem(cocheItem);
        }
        /// <summary>
        /// Actualiza un coche en la BBDD
        /// </summary>
        /// <param name="matricula">Matricula del coche a actualizar</param>
        /// <param name="cocheItem">Coche a actualizar</param>
        /// <returns>Coche actualizado</returns>
        public async Task<ActionResult<CocheItem>> PutCocheItem(string matricula, CocheItem cocheItem)
        {
            cocheItem.Matricula = cocheItem.Matricula.ToLower();

            if (!await _cocheRepository.CocheExist(matricula))
            {
                throw new AppException("El coche no existe en la BBDD.");
            }

            if (cocheItem.Paquetes.Any())
            {
                foreach (var paquete in cocheItem.Paquetes)
                {
                    await _paqueteRepository.PutPaqueteItem(paquete);
                }
            }

            if (cocheItem.Localizaciones.Any())
            {
                foreach (var localization in cocheItem.Localizaciones)
                {
                    await _coordenadaRepository.PutCoordenadaItem(localization);
                }
            }

            return await _cocheRepository.PutCocheItem(cocheItem);
        }
        /// <summary>
        /// Añade un paquete al coche indicado
        /// </summary>
        /// <param name="matricula">Matricula del coche</param>
        /// <param name="paqueteItem">Paquete a añadir</param>
        /// <returns>El coche con sus paquetes</returns>
        public async Task<ActionResult<CocheItem>> PutPaqueteItem(string matricula, PaqueteItem paqueteItem)
        {
            matricula = matricula.ToLower();

            if (!await _cocheRepository.CocheExist(matricula))
            {
                throw new AppException("El coche no existe en la BBDD.");
            }

            var coche = _cocheRepository.GetCocheItem(matricula).Result.Value;

            if (coche.Paquetes == null)
            {
                coche.Paquetes = new List<PaqueteItem>();
            }

            coche.Paquetes.Add(paqueteItem);

            return await _cocheRepository.PutCocheItem(coche);
        }
        /// <summary>
        /// Elimina un paquete del coche indicado
        /// </summary>
        /// <param name="matricula">Matricula del coche</param>
        /// <param name="paqueteItem">Paquete a eliminar</param>
        /// <returns>El coche con los paquetes que tiene</returns>
        public async Task<ActionResult<CocheItem>> DeletePaqueteItem(string matricula, PaqueteItem paqueteItem)
        {
            matricula = matricula.ToLower();

            if (!await _cocheRepository.CocheExist(matricula))
            {
                throw new AppException("El coche no existe en la BBDD.");
            }

            var coche = _cocheRepository.GetCocheItem(matricula).Result.Value;

            if (coche.Paquetes == null)
            {
                throw new AppException("Este coche no tiene ningún paquete.");
            }

            if (coche.Paquetes.Any(x=> x.Id == paqueteItem.Id))
            {
                coche.Paquetes.Remove(paqueteItem);
            }
            else
            {
                throw new AppException("El paquete a eliminar no coincide con el coche indicado");
            }

            return await _cocheRepository.PutCocheItem(coche);
        }
        /// <summary>
        /// Actualiza las coordenadas de un vehiculo
        /// </summary>
        /// <param name="matriculaCoordenada">Matricula del vehiculo</param>
        /// <param name="coordenada">Datos de la coordenada</param>
        /// <returns>Coche actualizado</returns>
        public async Task<ActionResult<CocheItem>> PutCoordenadaItem(string matriculaCoordenada, CoordenadaItem coordenada)
        {
            matriculaCoordenada = matriculaCoordenada.ToLower();

            if (!await _cocheRepository.CocheExist(matriculaCoordenada))
            {
                throw new AppException("El coche no existe en la BBDD.");
            }

            var coche = _cocheRepository.GetCocheItem(matriculaCoordenada).Result.Value;

            if (coche.Localizaciones == null)
            {
                coche.Localizaciones = new List<CoordenadaItem>();
            }

            coche.Localizaciones.Add(coordenada);

            return await _cocheRepository.PutCocheItem(coche);
        }
    }
}
