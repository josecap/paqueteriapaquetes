﻿using Microsoft.AspNetCore.Mvc;
using Paqueteria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paqueteria.Manager
{
    public interface ICoordenadaManager
    {
        Task<ActionResult<IEnumerable<CoordenadaItem>>> GetCoordenadas();
        Task<ActionResult<CoordenadaItem>> PutCoordenadaItem(CoordenadaItem coordenada);
        Task<ActionResult<CoordenadaItem>> PostCoordenadaItem(CoordenadaItem coordenada);
        Task<ActionResult<CoordenadaItem>> DeleteCoordenadaItem(CoordenadaItem coordenada);
    }
}
