﻿using Microsoft.AspNetCore.Mvc;
using Paqueteria.Models;
using Paqueteria.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paqueteria.Manager
{
    public class CoordenadaManager : ICoordenadaManager
    {
        private readonly ICoordenadaRepository _coordenadaRepository;

        public CoordenadaManager(ICoordenadaRepository coordenadaRepository)
        {
            _coordenadaRepository = coordenadaRepository;
        }
        /// <summary>
        /// Borra un coordenada de la BBDD
        /// </summary>
        /// <param name="coordenada">Coordenada a borrar</param>
        /// <returns>Coordenada borrada</returns>
        public async Task<ActionResult<CoordenadaItem>> DeleteCoordenadaItem(CoordenadaItem coordenada)
        {
            if (!await _coordenadaRepository.CoordenadaExist(coordenada.Id))
            {
                throw new ApplicationException("No hay una coordenada con esa id en BBDD");
            }

            var deletedCoordenada = await _coordenadaRepository.DeleteCoordenadaItem(coordenada);

            return deletedCoordenada;
        }
        /// <summary>
        /// Obtiene todas las coordenadas en BBDD
        /// </summary>
        /// <returns></returns>
        public Task<ActionResult<IEnumerable<CoordenadaItem>>> GetCoordenadas()
        {
            return _coordenadaRepository.GetCoordenadas();
        }
        /// <summary>
        /// Inserta una coordenada en BBDD
        /// </summary>
        /// <param name="coordenada">Coordenada a insertar</param>
        /// <returns></returns>
        public async Task<ActionResult<CoordenadaItem>> PostCoordenadaItem(CoordenadaItem coordenada)
        {
            if (await _coordenadaRepository.CoordenadaExist(coordenada.Id))
            {
                throw new ApplicationException("Ya hay una coordenada con esa id en BBDD");
            }

            return await _coordenadaRepository.PostCoordenadaItem(coordenada);
        }
        /// <summary>
        /// Actualiza una coordenada de la BBDD
        /// </summary>
        /// <param name="coordenada">Coordenada a actualizar</param>
        /// <returns></returns>
        public async Task<ActionResult<CoordenadaItem>> PutCoordenadaItem(CoordenadaItem coordenada)
        {
            if (!await _coordenadaRepository.CoordenadaExist(coordenada.Id))
            {
                throw new ApplicationException("No hay una coordenada con esa id en BBDD");
            }
            return await _coordenadaRepository.PostCoordenadaItem(coordenada);
        }
    }
}
