﻿using Microsoft.AspNetCore.Mvc;
using Paqueteria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paqueteria.Manager
{
    public interface ICocheManager
    {
        Task<ActionResult<IEnumerable<CocheItem>>> GetCoches();
        Task<ActionResult<CocheItem>> GetCocheItem(string matricula);
        Task<ActionResult<CocheItem>> PutCocheItem(string matricula, CocheItem cocheItem);
        Task<ActionResult<CocheItem>> PostCocheItem(CocheItem cocheItem);
        Task<ActionResult<CocheItem>> DeleteCocheItem(string matricula);
        Task<ActionResult<CocheItem>> PutPaqueteItem(string matricula, PaqueteItem paqueteItem);
        Task<ActionResult<CocheItem>> PutCoordenadaItem(string matriculaCoordenada, CoordenadaItem coordenada);
        Task<ActionResult<CocheItem>> DeletePaqueteItem(string matricula, PaqueteItem paqueteItem);
    }
}
