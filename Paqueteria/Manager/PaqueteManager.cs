﻿using Microsoft.AspNetCore.Mvc;
using Paqueteria.Exceptions;
using Paqueteria.Models;
using Paqueteria.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Paqueteria.Manager
{
    public class PaqueteManager : IPaqueteManager
    {
        private readonly IPaqueteRepository _paqueteRepository;
        private readonly ICocheRepository _cocheRepository;
        public PaqueteManager(IPaqueteRepository paqueteRepository, ICocheRepository cocheRepository)
        {
            _paqueteRepository = paqueteRepository;
            _cocheRepository = cocheRepository;
        }
        /// <summary>
        /// Borra un paquete de la base de datos
        /// </summary>
        /// <param name="numeroPaquete"></param>
        /// <returns></returns>
        public Task<ActionResult<PaqueteItem>> DeletePaqueteItem(string numeroPaquete)
        {
            if (!_paqueteRepository.PaqueteItemExists(numeroPaquete))
            {
                throw new AppException("El paquete no existe en la BBDD.");
            }

            return _paqueteRepository.DeletePaqueteItem(numeroPaquete);
        }
        /// <summary>
        /// Obtiene un paquete de la BBDD
        /// </summary>
        /// <param name="numeroPaquete">Numero de paquete a obtener</param>
        /// <returns></returns>
        public async Task<ActionResult<PaqueteItem>> GetPaqueteByNumeroPaquete(string numeroPaquete)
        {
            var paqueteItem = await _paqueteRepository.GetPaqueteByNumeroPaquete(numeroPaquete);

            if (paqueteItem == null)
            {
                throw new KeyNotFoundException("No existe el paquete en la base de datos.");
            }

            return paqueteItem;
        }

        /// <summary>
        /// Retorna, si existe, los datos del paquete que coincidan con el numero.
        /// </summary>
        /// <param name="numeroPaquete"></param>
        /// <returns></returns>
        public async Task<ActionResult<CocheItem>> GetCocheByNumeroPaquete(string numeroPaquete)
        {
            var paqueteItem = _paqueteRepository.GetPaqueteByNumeroPaquete(numeroPaquete).Result.Value;

            if (paqueteItem == null)
            {
                throw new KeyNotFoundException("No existe el paquete en la base de datos.");
            }

            return await _cocheRepository.GetCocheById(paqueteItem.CocheItemId);
        }
        /// <summary>
        /// Obtiene todos los paquetes en BBDD
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult<IEnumerable<PaqueteItem>>> GetPaqueteItems()
        {
            return await _paqueteRepository.GetPaqueteItems();
        }
        /// <summary>
        /// Inserta un paquete en BBDD
        /// </summary>
        /// <param name="paqueteItem">Paquete a insertar</param>
        /// <returns></returns>
        public Task<ActionResult<PaqueteItem>> PostPaqueteItem(PaqueteItem paqueteItem)
        {
            if (_paqueteRepository.PaqueteItemExists(paqueteItem.NumeroPaquete))
            {
                throw new AppException("El paquete ya existe en la BBDD.");
            }

            return _paqueteRepository.PostPaqueteItem(paqueteItem);
        }
        /// <summary>
        /// Actualiza un paquete de la BBDD
        /// </summary>
        /// <param name="numeroPaquete">Numero de paquete a actualizar</param>
        /// <param name="paqueteItem">Datos del paquete a actualizar</param>
        /// <returns></returns>
        public async Task<ActionResult<PaqueteItem>> PutPaqueteItem(string numeroPaquete, PaqueteItem paqueteItem)
        {
            if (numeroPaquete != paqueteItem.NumeroPaquete)
            {
                throw new AppException("El número de paquete no coincide con el paquete a modificar.");
            }

            if (!_paqueteRepository.PaqueteItemExists(paqueteItem.NumeroPaquete))
            {
                throw new AppException("El paquete a modificar no existe en la BBDD.");
            }

            return await _paqueteRepository.PutPaqueteItem(paqueteItem);
        }
    }
}
