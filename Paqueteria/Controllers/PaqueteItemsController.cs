﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Paqueteria.Manager;
using Paqueteria.Models;
using Paqueteria.Repository;

namespace Paqueteria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaqueteItemsController : ControllerBase
    {
        private readonly IPaqueteManager _paqueteManager;

        public PaqueteItemsController(IPaqueteManager paqueteManager)
        {
            _paqueteManager = paqueteManager;
        }

        /// <summary>
        /// Retorna todos los paquetes en base de datos.
        /// </summary>
        /// <returns>Todos los paquetes en BBDD</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PaqueteItem>>> GetPaqueteItems()
        {
            return await _paqueteManager.GetPaqueteItems();
        }

        /// <summary>
        /// Retorna, si existe, los datos del paquete que coincidan con el numero.
        /// </summary>
        /// <param name="numeroPaquete">Numero de paquete a buscar</param>
        /// <returns>Paquete que coincida con el número.</returns>
        [HttpGet("{numeroPaquete}")]
        public async Task<ActionResult<PaqueteItem>> GetPaqueteByNumeroPaquete(string numeroPaquete)
        {
            return await _paqueteManager.GetPaqueteByNumeroPaquete(numeroPaquete);
        }

        [HttpGet("coche/{numeropaquete}")]
        public async Task<ActionResult<CocheItem>> GetCocheByNumeroPaquete(string numeroPaquete)
        {
            return await _paqueteManager.GetCocheByNumeroPaquete(numeroPaquete);
        }

        /// <summary>
        /// Actualiza un paquete en la base de datos.
        /// </summary>
        /// <param name="numeroPaquete">Numero de paquete a actualizar</param>
        /// <param name="paqueteItem">Datos del paquete a actualizar</param>
        /// <returns>Paquete actualizado</returns>
        [HttpPut("{numeroPaquete}")]
        public async Task<ActionResult<PaqueteItem>> PutPaqueteItem(string numeroPaquete, PaqueteItem paqueteItem)
        {
            return await _paqueteManager.PutPaqueteItem(numeroPaquete, paqueteItem);
        }

        /// <summary>
        /// Inserta un paquete en base de datos
        /// </summary>
        /// <param name="paqueteItem">Paquete a insertar</param>
        /// <returns>Paquete insertado</returns>
        [HttpPost]
        public async Task<ActionResult<PaqueteItem>> PostPaqueteItem(PaqueteItem paqueteItem)
        {
            return await _paqueteManager.PostPaqueteItem(paqueteItem);
        }

        /// <summary>
        /// Borra un paquete de la base de datos
        /// </summary>
        /// <param name="numeroPaquete">Numero de paquete a borrar</param>
        /// <returns>Paquete borrado</returns>
        [HttpDelete("{numeroPaquete}")]
        public async Task<ActionResult<PaqueteItem>> DeletePaqueteItem(string numeroPaquete)
        {
            return await _paqueteManager.DeletePaqueteItem(numeroPaquete);
        }
    }
}
