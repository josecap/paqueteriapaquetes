﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Paqueteria.Manager;
using Paqueteria.Models;
using Paqueteria.Repository;

namespace Paqueteria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CocheItemsController : ControllerBase
    {
        private readonly ICocheManager _cocheManager;

        public CocheItemsController(ICocheManager cocheManager)
        {
            _cocheManager = cocheManager;
        }

        /// <summary>
        /// Devuelve todos los coches en BBDD
        /// </summary>
        /// <returns>Coches en BBDD</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CocheItem>>> GetCocheItems()
        {
            return await _cocheManager.GetCoches();
        }

        /// <summary>
        /// Devuelve el coche con l matricula indicada si existe
        /// </summary>
        /// <param name="matricula">matricula del coche a buscar</param>
        /// <returns>Coche</returns>
        [HttpGet("{matricula}")]
        public async Task<ActionResult<CocheItem>> GetCocheItem(string matricula)
        {
            return await _cocheManager.GetCocheItem(matricula);
        }

        /// <summary>
        /// Actualiza el coche indicado
        /// </summary>
        /// <param name="matricula">Matricula del coche a actualizar</param>
        /// <param name="cocheItem">Datos del coche a actualizar</param>
        /// <returns>Coche actualizado</returns>
        [HttpPut("{matricula}")]
        public async Task<ActionResult<CocheItem>> PutCocheItem(string matricula, CocheItem cocheItem)
        {
            return await _cocheManager.PutCocheItem(matricula, cocheItem);
        }
        /// <summary>
        /// Inserta un paquete al coche indicado con la matricula.
        /// </summary>
        /// <param name="matriculaPaquete">Matricula del coche</param>
        /// <param name="paqueteItem">Paquete a insertar</param>
        /// <returns>Coche actualizado</returns>
        [HttpPut("AddPaquete/{matriculaPaquete}")]
        public async Task<ActionResult<CocheItem>> PutPaqueteCoche(string matriculaPaquete, PaqueteItem paqueteItem)
        {
            return await _cocheManager.PutPaqueteItem(matriculaPaquete, paqueteItem);
        }
        /// <summary>
        /// Añade una coordenada a un vehiculo
        /// </summary>
        /// <param name="matriculaCoordenada">Matricula del vehiculo a añadir la coordenada</param>
        /// <param name="coordenada">Coordenada a añadir</param>
        /// <returns>coche con las coordenadas actualizadas</returns>
        [HttpPut("AddLocation/{matriculaCoordenada}")]
        public async Task<ActionResult<CocheItem>> PutPaqueteCoche(string matriculaCoordenada, CoordenadaItem coordenada)
        {
            return await _cocheManager.PutCoordenadaItem(matriculaCoordenada, coordenada);
        }
        /// <summary>
        /// Borra un paquete al coche indicado con la matricula.
        /// </summary>
        /// <param name="matriculaPaquete">Matricula del coche a borrar un paquete</param>
        /// <param name="paqueteItem">Datos del paquete</param>
        /// <returns>Datos del coche al que se le ha borrado un paquete</returns>
        [HttpPut("DeletePaquete/{matriculaPaquete}")]
        public async Task<ActionResult<CocheItem>> DeletePaqueteCoche(string matriculaPaquete, PaqueteItem paqueteItem)
        {
            return await _cocheManager.DeletePaqueteItem(matriculaPaquete, paqueteItem);
        }

        /// <summary>
        /// Insrta un coche en BBDD
        /// </summary>
        /// <param name="cocheItem">Datos del coche a insertar</param>
        /// <returns>Coche insertado</returns>
        [HttpPost]
        public async Task<ActionResult<CocheItem>> PostCocheItem(CocheItem cocheItem)
        {
            return await _cocheManager.PostCocheItem(cocheItem);
        }

        /// <summary>
        /// Borra el coche con la matricula indicada
        /// </summary>
        /// <param name="matricula">Matricula del coche a borrar</param>
        /// <returns>Coche borrado</returns>
        [HttpDelete("{matricula}")]
        public async Task<ActionResult<CocheItem>> DeleteCocheItem(string matricula)
        {
            return await _cocheManager.DeleteCocheItem(matricula);
        }
    }
}
