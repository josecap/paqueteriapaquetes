﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Paqueteria.Exceptions;
using Paqueteria.Models;
using Paqueteria.Repository;
using PaqueteriaTest.Asserts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Paqueteria.Manager.Test
{
    [TestClass()]
    public class CocheManagerTest
    {
        private Mock<ICocheRepository> _cocheRepositoryMock;
        private Mock<IPaqueteRepository> _paqueteRepositoryMock;
        private Mock<ICoordenadaRepository> _coordenadaRepositoryMock;
        public CocheManagerTest()
        {
            _cocheRepositoryMock = new Mock<ICocheRepository>();
            _paqueteRepositoryMock = new Mock<IPaqueteRepository>();
            _coordenadaRepositoryMock = new Mock<ICoordenadaRepository>();
        }

        [TestMethod()]
        public void DeleteCocheItemTestShuldWork()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.GetCocheItem(It.IsAny<string>())).ReturnsAsync(CocheAsserts.CocheItemAssert());

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var coche = cocheManager.DeleteCocheItem("0818dpn");

            //Assert
            Assert.IsNotNull(coche);
        }

        [TestMethod()]
        [ExpectedException(typeof(AppException))]
        public void DeleteCocheItemTestShouldThrowException()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.GetCocheItem(It.IsAny<string>())).ReturnsAsync(() => null);

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var coche = cocheManager.DeleteCocheItem("0818dpn");

            //Assert Expected Exception.
        }

        [TestMethod()]
        public void GetCocheItemTestShouldWork()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.GetCocheItem(It.IsAny<string>())).ReturnsAsync(CocheAsserts.CocheItemAssert());

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var coche = cocheManager.GetCocheItem("0818dpn");

            //Assert
            Assert.IsNotNull(coche);
        }

        [TestMethod()]
        [ExpectedException(typeof(AppException))]
        public void GetCocheItemTestShouldThrowException()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.GetCocheItem(It.IsAny<string>())).ReturnsAsync(() => null);

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var coche = cocheManager.GetCocheItem("0818dpn");

            //Assert Expected Exception.
        }

        [TestMethod()]
        public void GetCochesTestShouldWork()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.GetCoches()).ReturnsAsync(CocheAsserts.CochesItemsAssert());

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var coche = cocheManager.GetCoches();

            //Assert
            Assert.IsNotNull(coche);
        }

        [TestMethod()]
        public async Task PostCocheItemTestShouldWork()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.PostCocheItem(It.IsAny<CocheItem>())).ReturnsAsync(CocheAsserts.CocheItemAssert());
            _cocheRepositoryMock.Setup(x => x.CocheExist(It.IsAny<string>())).ReturnsAsync(false);
            _paqueteRepositoryMock.Setup(x => x.PostPaqueteItem(It.IsAny<PaqueteItem>()));
            _coordenadaRepositoryMock.Setup(x => x.PostCoordenadaItem(It.IsAny<CoordenadaItem>()));

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var coche = await cocheManager.PostCocheItem(CocheAsserts.CocheItemAssert());

            //Assert
            Assert.IsNotNull(coche);
        }

        [TestMethod()]
        [ExpectedException(typeof(AppException))]
        public async Task PostCocheItemTestShouldThrowException()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.CocheExist(It.IsAny<string>())).ReturnsAsync(true);

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var coche = await cocheManager.PostCocheItem(CocheAsserts.CocheItemAssert());

            //Assert Expected Exception
        }

        [TestMethod()]
        public async Task PutCocheItemTestShouldWork()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.PutCocheItem(It.IsAny<CocheItem>())).ReturnsAsync(CocheAsserts.CocheItemAssert());
            _cocheRepositoryMock.Setup(x => x.CocheExist(It.IsAny<string>())).ReturnsAsync(true);
            _paqueteRepositoryMock.Setup(x => x.PutPaqueteItem(It.IsAny<PaqueteItem>()));
            _coordenadaRepositoryMock.Setup(x => x.PutCoordenadaItem(It.IsAny<CoordenadaItem>()));

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var coche = await cocheManager.PutCocheItem("0818dpn", CocheAsserts.CocheItemAssert());

            //Assert
            Assert.IsNotNull(coche);
        }

        [TestMethod()]
        [ExpectedException(typeof(AppException))]
        public async Task PutCocheItemTestShoulThrowException()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.CocheExist(It.IsAny<string>())).ReturnsAsync(false);

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var coche = await cocheManager.PutCocheItem("0818dpn", CocheAsserts.CocheItemAssert());

            //Assert Expected Exception
        }

        [TestMethod()]
        public async Task PutPaqueteItemTestShouldWork()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.PutCocheItem(It.IsAny<CocheItem>())).ReturnsAsync(CocheAsserts.CocheItemAssert());
            _cocheRepositoryMock.Setup(x => x.GetCocheItem(It.IsAny<string>())).ReturnsAsync(CocheAsserts.CocheItemAssert());
            _cocheRepositoryMock.Setup(x => x.CocheExist(It.IsAny<string>())).ReturnsAsync(true);

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var paquete = await cocheManager.PutPaqueteItem("0818dpn", PaqueteAsserts.PaqueteItemAssert());

            //Assert
            Assert.IsNotNull(paquete);
        }

        [TestMethod()]
        [ExpectedException(typeof(AppException))]
        public async Task PutPaqueteItemTestShouldThrowException()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.CocheExist(It.IsAny<string>())).ReturnsAsync(false);

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var paquete = await cocheManager.PutPaqueteItem("0818dpn", PaqueteAsserts.PaqueteItemAssert());

            //Assert Expected Exception
        }

        [TestMethod()]
        public async Task DeletePaqueteItemTestShouldWork()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.PutCocheItem(It.IsAny<CocheItem>())).ReturnsAsync(CocheAsserts.CocheItemAssert());
            _cocheRepositoryMock.Setup(x => x.GetCocheItem(It.IsAny<string>())).ReturnsAsync(CocheAsserts.CocheItemAssert());
            _cocheRepositoryMock.Setup(x => x.CocheExist(It.IsAny<string>())).ReturnsAsync(true);

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var paquete = await cocheManager.DeletePaqueteItem("0818dpn", PaqueteAsserts.PaqueteItemAssert());

            //Assert
            Assert.IsNotNull(paquete);
        }

        [TestMethod()]
        [ExpectedException(typeof(AppException))]
        public async Task DeletePaqueteItemTestShoulThrowException()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.CocheExist(It.IsAny<string>())).ReturnsAsync(false);

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var paquete = await cocheManager.DeletePaqueteItem("0818dpn", PaqueteAsserts.PaqueteItemAssert());

            //Assert Expected Exception
        }

        [TestMethod()]
        [ExpectedException(typeof(AppException))]
        public async Task DeletePaqueteItemTestShoulThrowException2()
        {
            //Arrange
            _cocheRepositoryMock.Setup(x => x.PutCocheItem(It.IsAny<CocheItem>())).ReturnsAsync(CocheAsserts.CocheItemAssert());
            _cocheRepositoryMock.Setup(x => x.GetCocheItem(It.IsAny<string>())).ReturnsAsync(CocheAsserts.CocheItemAssert());
            _cocheRepositoryMock.Setup(x => x.CocheExist(It.IsAny<string>())).ReturnsAsync(true);

            CocheManager cocheManager = new CocheManager(_cocheRepositoryMock.Object, _paqueteRepositoryMock.Object, _coordenadaRepositoryMock.Object);
            //Act
            var paquete = await cocheManager.DeletePaqueteItem("0818dpn", PaqueteAsserts.PaqueteItemAssertThrowException());

            //Assert Expected Exception
        }
    }
}