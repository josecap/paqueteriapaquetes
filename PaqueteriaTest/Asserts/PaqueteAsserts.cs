﻿using Paqueteria.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaqueteriaTest.Asserts
{
    public static class PaqueteAsserts
    {
        public static PaqueteItem PaqueteItemAssert()
        {
            return new PaqueteItem
            {
                CocheItemId = 1,
                Id = 1,
                NumeroPaquete = "PK546546654"
            };
        }

        public static PaqueteItem PaqueteItemAssertThrowException()
        {
            return new PaqueteItem
            {
                CocheItemId = 1,
                Id = 2,
                NumeroPaquete = "PK546546654"
            };
        }
    }
}
