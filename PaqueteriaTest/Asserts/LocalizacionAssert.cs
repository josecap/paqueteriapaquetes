﻿using Paqueteria.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaqueteriaTest.Asserts
{
    public static class LocalizacionAssert
    {
        public static CoordenadaItem CoordenadaItemAssert()
        {
            return new CoordenadaItem
            {
                Id = 1,
                HorasX = 1,
                MinutosX = 2,
                SegundosX = 1,
                OrientacionX = "N",
                HorasY = 2,
                MinutosY = 2,
                SegundosY = 3,
                OrientacionY = "O",
                FechaActualizacion = DateTime.Now
            };
        }
    }
}
