﻿using Paqueteria.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaqueteriaTest.Asserts
{
    public static class CocheAsserts
    {
        public static CocheItem CocheItemAssert()
        {
            return new CocheItem
            {
                Id = 1,
                Matricula = "0818dpn",
                Paquetes = new List<PaqueteItem>()
                {
                    new PaqueteItem
                    {
                        CocheItemId = 1,
                        Id = 1,
                        NumeroPaquete = "PK546546654"
                    }
                },
                Localizaciones = new List<CoordenadaItem>()
                {
                    new CoordenadaItem
                    {
                        Id = 1,
                        HorasX = 1,
                        MinutosX = 2,
                        SegundosX = 1,
                        OrientacionX = "N",
                        HorasY = 2,
                        MinutosY = 2,
                        SegundosY = 3,
                        OrientacionY = "O",
                        FechaActualizacion = DateTime.Now
                    }
                }
            };
        }

        public static List<CocheItem> CochesItemsAssert()
        {
            return new List<CocheItem>()
            {
                new CocheItem
                {
                    Id = 1,
                    Matricula = "0818dpn",
                    Paquetes = new List<PaqueteItem>()
                    {
                        new PaqueteItem
                        {
                            CocheItemId = 1,
                            Id = 1,
                            NumeroPaquete = "PK546546654"
                        }
                    },
                    Localizaciones = new List<CoordenadaItem>()
                    {
                        new CoordenadaItem
                        {
                            Id = 1,
                            HorasX = 1,
                            MinutosX = 2,
                            SegundosX = 1,
                            OrientacionX = "N",
                            HorasY = 2,
                            MinutosY = 2,
                            SegundosY = 3,
                            OrientacionY = "O",
                            FechaActualizacion = DateTime.Now
                        }
                    }
                },
                new CocheItem
                {
                    Id = 1,
                    Matricula = "0256GTP",
                    Paquetes = new List<PaqueteItem>()
                    {
                        new PaqueteItem
                        {
                            CocheItemId = 1,
                            Id = 1,
                            NumeroPaquete = "PK546546878"
                        }
                    },
                    Localizaciones = new List<CoordenadaItem>()
                    {
                        new CoordenadaItem
                        {
                            Id = 1,
                            HorasX = 1,
                            MinutosX = 2,
                            SegundosX = 1,
                            OrientacionX = "S",
                            HorasY = 2,
                            MinutosY = 5,
                            SegundosY = 3,
                            OrientacionY = "E",
                            FechaActualizacion = DateTime.Now
                        }
                    }
                }
            };
        }
    }
}
